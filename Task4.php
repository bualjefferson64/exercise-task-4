<?php
    class API {
        public function printFullName($fullName) {
            echo "Full Name: $fullName <br>";
        }

        public function printHobbies($hobbies) {
            echo "Hobbies: <br>";
            foreach ($hobbies as $hobby) {
                echo "  $hobby <br>";
            }
        }

        public function printInfo($info) {
           
            echo "Age: {$info->age} <br>";
            echo "Email: {$info->email} <br>";
            echo "Birthday: {$info->birthday} <br>";
        }
    }

    $fullName = "Jefferson C. Bual";
    $hobbies = ["Reading", "Gaming", "Watching movie", "Coding"];
    $info = (object) [
        "age" => 21,
        "email" => "bualjefferson64@gmail.com",
        "birthday" => "July 12, 2002"
    ];

    // Create an instance of the API class
    $api = new API();

    // Call the functions
    $api->printFullName($fullName);
    echo "<br>";
    $api->printHobbies($hobbies);
    echo "<br>";
    $api->printInfo($info);
?>
